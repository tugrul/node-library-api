
const {Schema} = require('mongoose');
const nameValidator = require('../validators/nameValidator');

const schema = new Schema({
    name: {type: String, required: true/*, validate: nameValidator*/},
    borrows: [{type: Schema.Types.ObjectId, ref: 'Borrow'}]
});

// bind book model on connection pool
module.exports = ({mongoose}) => mongoose.model('Book', schema);

