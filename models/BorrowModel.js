
const {Schema} = require('mongoose');


const schema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    book: { type: Schema.Types.ObjectId, ref: 'Book', required: true },
    tookAt: {type: Date, default: Date.now},
    returnedAt: {type: Date},
    rating: {type: Number, min:0, max: 10}
});

// prevent duplicate borrows at the same time
schema.index({book: 1, returnedAt: 1}, {unique: true});

// bind user model on connection pool
module.exports = ({mongoose}) => mongoose.model('Borrow', schema);
