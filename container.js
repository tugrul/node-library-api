
const {MONGODB_URL} = process.env;

const {createContainer, Lifetime, asClass, asValue, asFunction} = require('awilix');
const mongoose = require('mongoose');

const container = createContainer();

container.register({
    mongoose: asFunction(() => mongoose.createConnection(MONGODB_URL, { useNewUrlParser: true }))
        .singleton().disposer(connection => connection.close())
});

container.loadModules([
        ['models/**/*.js', { register: asFunction, lifetime: Lifetime.SINGLETON}],
        'controllers/**/*.js'
    ], {
        formatName: 'camelCase',
        resolverOptions: {
            register: asClass,
            lifetime: Lifetime.SINGLETON
        }
    }
);


module.exports = container;
