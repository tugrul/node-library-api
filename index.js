
require('dotenv').config();

const {APP_PORT} = process.env;

const express = require('express');
const bodyParser = require('body-parser');

const container = require('./container');

const app = express();

app.use(bodyParser.json());

const userController = container.resolve('userController');
app.get('/users', userController.getUsers);
app.get('/users/:id', userController.getUser);
app.post('/users', userController.createUser);
app.post('/users/:id', userController.updateUser);

app.post('/users/:userId/borrow/:bookId', userController.borrowBook);
app.post('/users/:userId/return/:bookId', userController.returnBook);

const bookController = container.resolve('bookController');
app.get('/books', bookController.getBooks);
app.get('/books/:id', bookController.getBook);
app.post('/books', bookController.createBook);


app.listen(APP_PORT, () => console.log(`Library API listening on port ${APP_PORT}!`));

