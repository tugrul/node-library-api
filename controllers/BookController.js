
class BookController {

    constructor({bookModel}) {
        this.BookModel = bookModel;

        this.getBooks = this.getBooks.bind(this);
        this.getBook = this.getBook.bind(this);
        this.createBook = this.createBook.bind(this);
        this.updateBook = this.updateBook.bind(this);
    }

    calculateAverage(borrows) {
        const totalRating = borrows.reduce((prev, current) => prev + (current.rating || 0), 0);

        return {
            count: borrows.length,
            rating: totalRating / borrows.length
        };
    }

    getBooks(req, res) {
        this.BookModel.find()
            .populate({path: 'borrows', select: '-book', match: { returnedAt: { $exists: true } }})
            .then(books => books.map(book => ({id: book._id,
                name: book.name, borrows: this.calculateAverage(book.borrows)})))
            .then(books => res.json(books))
            .catch(err => res.status(500).json({message: err.message}))
    }

    getBook(req, res) {
        this.BookModel.findById(req.params.id)
            .populate({path: 'borrows', select: '-book', match: { returnedAt: { $exists: true } }})
            .then(book => {
                if (!book) {
                    throw new Error('There is no book by the id');
                }

                res.json({id: book._id,
                    name: book.name, borrows: this.calculateAverage(book.borrows)})
            })
            .catch(err => res.status(500).json({message: err.message}))
    }

    createBook(req, res) {
        this.BookModel.create(req.body)
            .then(doc => res.json(doc))
            .catch(err => res.status(500).json({message: err.message}))
    }

    updateBook(req, res) {
        this.BookModel.findByIdAndUpdate(req.params.id)
            .then(doc => res.json(doc))
            .catch(err => res.status(500).json({message: err.message}))
    }

}

module.exports = BookController;
