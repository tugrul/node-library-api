
class UserController {

    constructor({userModel, bookModel, borrowModel}) {
        this.UserModel = userModel;
        this.BookModel = bookModel;
        this.BorrowModel = borrowModel;

        this.getUsers = this.getUsers.bind(this);
        this.getUser = this.getUser.bind(this);
        this.createUser = this.createUser.bind(this);
        this.updateUser = this.updateUser.bind(this);
        this.borrowBook = this.borrowBook.bind(this);
        this.returnBook = this.returnBook.bind(this);
    }

    getUsers(req, res) {
        this.UserModel.find()
            .populate({path: 'borrows', select: '-user', populate: {path: 'book', select: '-borrows'}})
            .then(users => res.json(users))
            .catch(err => res.status(500).json({message: err.message}));
    }

    getUser(req, res) {
        this.UserModel.findById(req.params.id)
            .populate({path: 'borrows', select: '-user', populate: {path: 'book', select: '-borrows'}})
            .then(user => {
                if (!user) {
                    throw new Error('There is no user by the id');
                }

                res.json(user);
            })
            .catch(err => res.status(500).json({message: err.message}));
    }

    createUser(req, res) {
        this.UserModel.create(req.body)
            .then(doc => res.json(doc))
            .catch(err => res.status(500).json({message: err.message}));
    }

    updateUser(req, res) {
        this.UserModel.findByIdAndUpdate(req.params.id)
            .then(doc => res.json(doc))
            .catch(err => res.status(500).json({message: err.message}));
    }

    borrowBook(req, res) {

        const {userId, bookId} = req.params;

        Promise.all([
            this.UserModel.findById(userId),
            this.BookModel.findById(bookId)])
            .then(([user, book]) => this.BorrowModel.create({user: user._id, book:book._id})
                .then(borrow => {
                    user.borrows.push(borrow._id);
                    book.borrows.push(borrow._id);

                    return Promise.all([
                        user.save(), book.save()
                    ]).then(() => {
                        res.json(borrow);
                    });
                }))
            .catch(err => res.status(500).json({message: err.message}));

    }

    returnBook(req, res) {

        const {userId, bookId} = req.params;

        this.BorrowModel.findOneAndUpdate({user: userId, book: bookId, returnedAt: null},
            {returnedAt: new Date(), rating: req.body.score})
            .then(borrow => {
                if (!borrow) {
                    throw new Error('There is no borrow to return back');
                }

                res.json(borrow);
            })
            .catch(err => res.status(500).json({message: err.message}));
    }
}

module.exports = UserController;
